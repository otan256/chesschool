class RefactorGames < ActiveRecord::Migration
  def up
    add_column :games, :min_rating, :integer, :null => false
    add_column :games, :max_rating, :integer, :null => false
    add_column :games, :timer, :string
  end

  def down
  end
end
