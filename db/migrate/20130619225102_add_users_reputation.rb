class AddUsersReputation < ActiveRecord::Migration
  def up
    add_column :users, :reputation, :integer, default: 1
  end

  def down
  end
end
