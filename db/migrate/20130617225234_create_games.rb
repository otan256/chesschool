class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.text :board
      t.integer :winner_id
      t.string :timer
      t.integer :min_rating, :null => false
      t.integer :max_rating, :null => false
      t.text :game_history
      t.datetime :finished_at
      t.datetime :started_at

      t.timestamps
    end
  end
end
