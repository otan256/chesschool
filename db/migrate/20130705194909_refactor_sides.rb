class RefactorSides < ActiveRecord::Migration
  def up
    remove_column :gameplays, :side
    add_column :gameplays, :side, :string, default: 'white'
  end

  def down
  end
end
