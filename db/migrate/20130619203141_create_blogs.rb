class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.integer :author_id,     null: false
      t.text :content
      t.string :title
      t.integer :likes
      t.string :attachments

      t.timestamps
    end
  end
end
