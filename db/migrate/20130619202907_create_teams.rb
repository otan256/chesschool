class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name,       null: false
      t.string :avatar,     default: 'no_avatar.jpg'
      t.string :description
      t.integer :rating,    default: 1000
      t.integer :staff,     default: 1000
      t.integer :staff_2,   default: 10
      t.integer :glory,     default: 1
      t.boolean :active,    default: false
      t.integer :leader_id
      t.string :t_type, default: :clan

      t.timestamps
    end
  end
end
