class FixNews < ActiveRecord::Migration
  def up
    remove_column :news, :likes
    add_column :news, :likes, :integer, default: 0
    rename_column :news, :type, :n_type
  end

  def down
  end
end
