class CreateGameplays < ActiveRecord::Migration
  def up
    create_table :gameplays do |t|
      t.integer :user_id, :null => false
      t.integer :game_id, :null => false
      # 0 -- white | 1 -- black
      t.boolean :side, :default => '0'
      t.string :move_queue, :limit => 20
      t.boolean :email_notify, :default => true
      t.timestamps
    end
  end

  def down
    drop_table :gameplays
  end
end
