class RefactorGamesColumns < ActiveRecord::Migration
  def up
    rename_column :games, :data, :board
    remove_column :games, :users_count
    remove_column :games, :time_per_move
  end

  def down
  end
end
