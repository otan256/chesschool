require 'active_record'

class CreateMessages < ActiveRecord::Migration
  def change

    # m_types: message, request, auto
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :reciever_id
      t.string :title
      t.string :content
      t.string :attachment
      t.string :m_type,       default: :message

      t.timestamps
    end
  end
end
