class AddUsersTeam < ActiveRecord::Migration
  def up
    add_column :users, :team_id, :integer
    add_column :users, :rating, :integer, default: 1000
  end

  def down
  end
end