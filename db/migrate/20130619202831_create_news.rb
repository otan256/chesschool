class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.integer :author_id,   null: false
      t.string :type,         default: :news
      t.string :title
      t.text :content
      t.string :attachments
      t.integer :likes

      t.timestamps
    end
  end
end
