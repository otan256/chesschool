# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130716235519) do

  create_table "blogs", :force => true do |t|
    t.integer  "author_id",   :null => false
    t.text     "content"
    t.string   "title"
    t.integer  "likes"
    t.string   "attachments"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "gameplays", :force => true do |t|
    t.integer  "user_id",                                         :null => false
    t.integer  "game_id",                                         :null => false
    t.string   "move_queue",   :limit => 20
    t.boolean  "email_notify",               :default => true
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "side",                       :default => "white"
  end

  create_table "games", :force => true do |t|
    t.text     "board"
    t.integer  "winner_id"
    t.integer  "min_rating",   :null => false
    t.integer  "max_rating",   :null => false
    t.text     "game_history"
    t.datetime "finished_at"
    t.datetime "started_at"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "timer"
  end

  create_table "invitations", :force => true do |t|
    t.integer  "sender_id"
    t.string   "recipient_email"
    t.string   "token"
    t.datetime "sent_at"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "messages", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "reciever_id"
    t.string   "title"
    t.string   "content"
    t.string   "attachment"
    t.string   "m_type",      :default => "message"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "news", :force => true do |t|
    t.integer  "author_id",                       :null => false
    t.string   "n_type",      :default => "news"
    t.string   "title"
    t.text     "content"
    t.string   "attachments"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "likes",       :default => 0
  end

  create_table "rates", :force => true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.integer  "stars",         :null => false
    t.string   "dimension"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "rates", ["rateable_id", "rateable_type"], :name => "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], :name => "index_rates_on_rater_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "teams", :force => true do |t|
    t.string   "name",                            :null => false
    t.string   "avatar"
    t.string   "description"
    t.integer  "rating",      :default => 1000
    t.integer  "staff",       :default => 1000
    t.integer  "staff_2",     :default => 10
    t.integer  "glory",       :default => 1
    t.boolean  "active",      :default => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "leader_id"
    t.string   "t_type",      :default => "clan"
  end

  create_table "users", :force => true do |t|
    t.string   "last_name"
    t.string   "first_name"
    t.string   "password"
    t.string   "email"
    t.string   "phone"
    t.boolean  "active",                 :default => false
    t.string   "avatar",                 :default => "no_avatar.jpg"
    t.string   "settings"
    t.datetime "birth_date"
    t.string   "country"
    t.text     "about"
    t.string   "ip_address"
    t.string   "last_ip"
    t.string   "remember_me_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.datetime "last_seen_at"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.string   "security_code",                                       :null => false
    t.integer  "team_id"
    t.integer  "rating",                 :default => 1000
    t.integer  "reputation",             :default => 1
    t.integer  "invitation_id"
    t.integer  "invitation_limit",       :default => 10
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
