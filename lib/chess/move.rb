class Chess::Move

  attr_accessor :side, :match, :captured_piece_coord_sym, :castled
  attr_reader :board, :to_coord, :from_coord

  def initialize(coord, board)
    @from_coord = coord[:from]
    @to_coord = coord[:to]
    @board = board
  end

  def from_coord_sym;
    @from_coord && @from_coord.to_sym;
  end

  def to_coord_sym;
    @to_coord && @to_coord.to_sym;
  end

  def captured_piece_coord
    board[from_coord_sym]
  end

  def captured_piece_coord_sym
    @captured_piece_coord_sym = captured_piece_coord && captured_piece_coord.to_sym
  end

  def to_json
    h = {
        'from_coord' => @from_coord,
        'to_coord' => @to_coord
    }
    h.to_json
  end

end
