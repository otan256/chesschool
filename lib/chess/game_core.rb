class Chess::GameCore

  # The ranks, or rows, defined in layout order, with white to move.
  RANKS = %w{ 8 7 6 5 4 3 2 1 }

  # The files, or columns, defined in layout order, with white to move.
  FILES = %w{ a b c d e f g h }

  # The squares involved in an opening up an enpassant square
  EN_PASSANT = {:white => [2, 3, 4], :black => [7, 6, 5]}

  attr_accessor :board

  def initialize
    @board = Chess::Board.new
  end

  def self.restore_from_dump(board_dump)
    @game = self.new
    @board = board_dump
    @game
  end

  class << self

    def ranks(to_move = :white, order = :layout_order)
      to_move == :white ? RANKS : RANKS.reverse
    end

    def files(to_move = :white, order = :layout_order)
      to_move == :white ? FILES : FILES.reverse
    end

    def all_positions(to_move = :white_to_move, order = :layout_order)
      positions = []
      first_by, then_by = RANKS, FILES
      then_by.each do |outer|
        first_by.each do |inner|
          file, rank = (first_by == RANKS ? [outer, inner] : [inner, outer])
          positions << "#{file}#{rank}".to_sym
        end
      end
      positions
    end

    def valid_position? pos
      !pos.nil? && all_positions.include?(pos.to_sym)
    end

    def each_position to_move = :white_to_move, order = :layout_order
      all_positions(to_move, order).each { |p| yield p }
    end

  end # class << self
end
