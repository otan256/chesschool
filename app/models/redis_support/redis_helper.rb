module RedisSupport
  module RedisHelper

    def validate_redis_cache_key(key)
      raise "Incorrect KEY #{key}" unless key.gsub(/[a-zA-Z0-9 _\#\-\+\$\|\[\]]+/, '').blank?
      true
    end

    def compose_key
      "user_#{self.id}_last_seen_at"
    end

    def compose_key_for_game_timer
      "game_#{self.id}_timer"
    end

    def cache_put(key, value)
      validate_redis_cache_key key
      if value
        value = Marshal.dump value
        $redis.set(key, value)
      else
        cache_del key
      end
    end

    def cache_get(key, default = nil)
      value = $redis.get key
      autoload_missing_constants do
        value = Marshal.load value if value
      end

      value || default
    end

    def cache_del(key)
      validate_redis_cache_key key
      $redis.del key
    end

    private

    @@lazy_load ||= Hash.new { |hash, hash_key| hash[hash_key] = true; false }

    def autoload_missing_constants
      yield
    rescue ArgumentError => error
      if error.to_s[/undefined class|referred/] && !@@lazy_load[error.to_s.split.last.sub(/::$/, '').constantize] then
        retry
      else
        raise error
      end
    end


  end
end