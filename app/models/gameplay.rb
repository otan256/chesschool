class Gameplay < ActiveRecord::Base
  attr_accessible :user_id, :game_id, :side

  belongs_to :player, class_name: 'User', foreign_key: :user_id
  belongs_to :game

  SIDES = [['white', 'white'], ['black', 'black']]

  def play_as
    "chess/sets/default/king_#{self.side.first}.png"
  end

  def set_playing_side(side)
    self.side = side
    self.save!
  end

end