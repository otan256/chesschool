class News < ActiveRecord::Base
  attr_accessible :title, :content, :author_id, :attachments
  ajaxful_rateable :stars => 5, :dimensions => [:likes ], :allow_update => true

  belongs_to :user, foreign_key: :author_id
  belongs_to :team, foreign_key: :author_id

  scope :recent, where('created_at > ?', Time.now - 2.days)
  scope :interesting, where('likes > ?', 3)

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }

  validates_presence_of :title

  def author
    self.user
  end

  def save_one!(attr)
    self.author_id = attr[:author_id]
    self.title = attr[:title]
    self.content = attr[:content]
    self.likes = attr[:likes] if attr[:likes]
    self.n_type = attr[:n_type] if attr[:n_type]
    self.save!
  end

  def like!(stars, post, dimension)
    self.likes += 1 if stars >= 3
    self.save!
    self.rate(stars, post, dimension)
  end

end
