class Game < ActiveRecord::Base
  serialize :data, Hash

  include Rules::GameRules
  include RedisSupport::RedisHelper

  attr_accessible :timer, :max_rating, :min_rating

  TIMES_PER_MOVE = [1, 2, 3, 4, 5]
  TIMES_PER_GAME = [7, 10, 15, 30]

  validates_presence_of :min_rating, :max_rating

  # fetches the first and second joins to player, which are white, black respectively
  has_many :gameplays do
    def white;
      self[0];
    end

    def black;
      self[1];
    end
  end

  has_many :players, class_name: 'User', foreign_key: :user_id, through: :gameplays

  def create_available(opts, user)
    self.gameplays << Gameplay.new({side: opts[:side], user_id: user.id})
    self.timer = opts[:timer] if opts[:timer]
    self.save!
    cache_put compose_key_for_game_timer, opts[:timer]
  end

  def update_available(opts)
    self.gameplays.first.set_playing_side(opts[:side]) if opts[:side]
    self.min_rating = opts[:min_rating] if opts[:min_rating]
    self.max_rating = opts[:max_rating] if opts[:max_rating]
    self.timer = opts[:timer] if opts[:timer]
    self.save!
    cache_put compose_key_for_game_timer, opts[:timer]
  end

  def force_join!(user)
    self.gameplays << Gameplay.new(user_id: user.id)
    self.started_at = Time.now
    self.save!
    self
  end

  def start!
    if can_start_game?(self)
      @game_core = Chess::GameCore.new

      self.board = @game_core.board
      self.started_at = DateTime.now
      self.save!
    end
    self
  end


  def opponent(of_user)
    if self.players_count <= 1
      'Open'
    else
      self.players.where("users.id != #{of_user.id}").first.full_name
    end
  end

  def players_count
    self.players.count
  end

  def available_sits
    2 - players_count
  end

  def rating
    "#{self.min_rating} - #{self.max_rating}"
  end

  def side
    self.gameplays.first.side
  end

  def display_time
    cache_get(compose_key_for_game_timer)
  end

  def self.side_for_select
    Gameplay::SIDES
  end

  def self.times_for_select
    TIMES_PER_GAME.map { |t| ["#{t} / game", "#{t} / game"] }.concat TIMES_PER_MOVE.map { |t| ["#{t} / move", "#{t} / move"] }
  end

end
