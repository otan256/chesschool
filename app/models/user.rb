# coding: utf-8

# md5 is needed for pasword generation
require 'digest/md5'

class User < ActiveRecord::Base
  serialize :settings, Hash

  ajaxful_rater

  attr_accessible :first_name, :last_name, :phone, :email, :country, :birth_date, :password, :security_code, :invitation_token, :password_confirmation
  attr_writer :current_step

  include FileUtils
  include UserExt::Registration
  include UserExt::ProtectedAttributesMethods
  include UserExt::GamesMethods
  include RedisSupport::RedisHelper
  include Rules::UserRules

  has_many  :gameplays
  has_many :games, through: :gameplays
  belongs_to :team
  has_many :blogs, class_name: Blog, foreign_key: :author_id
  has_many :articles, class_name: News, foreign_key: :author_id, :conditions => ['type = article']
  has_many :messages, foreign_key: :reciever_id
  has_many :sent_invitations, :class_name => 'Invitation', :foreign_key => 'sender_id'
  belongs_to :invitation

  validates_presence_of :first_name, :last_name, :phone, :email, :password, :security_code
  validates_confirmation_of :password
  # validations are done on client

  before_create do
    generate_token(:remember_me_token)
  end

  def friends
    []
  end

  def invitation_token
    invitation.token if invitation
  end

  def invitation_token=(token)
    self.invitation = Invitation.find_by_token(token)
  end

  def current_step
    @current_step || steps.first.to_i
  end

  def steps
    [1, 2, 3, 4]
  end

  def first_step?
    current_step == steps.first
  end

  def game_side(game_id)
    side = self.gameplays.where(game_id: game_id).first.side
    side ? 'Black' : 'White'
  end

  def last_step?
    current_step == steps.last
  end

  def next_step
    current_step = @current_step + 1
  end

  def full_name
    if self.last_name
      self.first_name + ' ' + self.last_name
    else
      self.first_name
    end
  end

  def rating_range
    {min: self.rating - 125, max: self.rating + 155}
  end

  def sent_messages
    Message.where("sender_id = #{self.id}")
  end

  def online?
    if cache_get(compose_key)
      'online' if cache_get(compose_key).to_time > 10.minutes.ago
    end
  end

  def update_online
    cache_put compose_key, Time.now.to_s
    self.updated_at = Time.now
  end

  def self.online_users
    self.where('updated_at > ?', Time.now.utc - 10.minutes)
  end

  def update_edited(attr)
    self.first_name = attr[:first_name] unless attr[:first_name].blank?
    self.last_name = attr[:last_name] unless attr[:last_name].blank?
    self.phone = attr[:phone] unless attr[:phone].blank?
    self.about = attr[:about]
    self.country = attr[:country]
    birth_date = DateTime.new(attr['birth_date(1i)'].to_i, attr['birth_date(2i)'].to_i, attr['birth_date(3i)'].to_i)
    self.birth_date = birth_date if birth_date
    update_avatar!(attr[:avatar]) if attr[:avatar]
    self.save!
  end

  def update_avatar!(avat)
    name = "#{self.id.to_s}.jpg"
    File.open(Rails.root.join('public', 'tmp_avatars/users', name), 'wb') do |file|
      file.write(avat.read)
    end

    FileUtils.cp "public/tmp_avatars/users/#{name}", "#{Rails.root}/app/assets/images/avatars/users/#{name}"
    File.delete("public/tmp_avatars/users/#{name}") if File.exist?("public/tmp_avatars/users/#{name}")
    self.avatar = name
  end

  def force_join_to_team!(team)
    self.team_id = team.id
    self.save!
  end

  def in_team?
    self.team_id.present?
  end

  def valid_security_code?(code)
    self.security_code == code
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def generate_security_code
    SecureRandom.urlsafe_base64
  end

  def authenticate(user_mail, user_password)
    encrypted_password = User.encrypt_a_password(user_password)
    r = User.all :conditions => ['email = ? and password = ?', user_mail, encrypted_password]

    unless r
      encrypted_password = User.encrypt_a_password(user_password, true)
      r = User.all :conditions => ['email = ? and password = ?', user_mail, encrypted_password]
    end

    r
  end

  def send_team_join_request
    #TODO
  end

  def send_password_reset
    PasswordMailer.password_reset(self).deliver
  end

  def self.reputation=(value)
    raise 'Cannot set protected attribute!'
  end

  private
  # private class methods

  def set_invitation_limit
    self.invitation_limit = 10
  end

  def self.encrypt_a_password(password, case_sensitive = false)
    password = password.strip.mb_chars.downcase unless case_sensitive
    Digest::MD5.hexdigest(password)
  end

end
