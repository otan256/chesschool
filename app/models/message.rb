class Message < ActiveRecord::Base
  attr_accessible :title, :content, :sender_id, :reciever_id

  belongs_to :user
  belongs_to :team

  def save_one!(attr)
    self.sender_id = attr[:sender_id]
    self.reciever_id = attr[:reciever_id]
    self.title = attr[:title]
    self.content = attr[:content]
    self.save!
  end

  def sender
    User.find_by_id self.sender_id
  end

  def reciever
    User.find_by_id self.reciever_id
  end


end
