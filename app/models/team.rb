class Team < ActiveRecord::Base
  attr_accessible :name, :avatar, :description

  include Rules::TeamRules

  LEADER_SIGN = 'L'
  TEAM_CHANGE_REPUTATION_DECREASE = 10
  USERS_COUNT_TO_BECOME_ACTIVE = 3

  has_many :users
  has_many :blogs, class_name: Blog, foreign_key: :author_id
  has_many :articles, class_name: News, foreign_key: :author_id, :conditions => ['type = article']
  has_many :messages, foreign_key: :sender_id

  validates_presence_of :name, :description

  def active?
    self.active
  end

  def save_team(attr)
    self.name = attr[:name] if attr[:name]
    self.descriptions = attr[:descriptions] if attr[:descriptions]
    if attr[:avatar] && !self.new_record?
      update_avatar!(attr[:avatar])
    else
      self.avatar = 'no_avatar.jpg'
    end
    self.save!
  end

  def update_avatar!(avat)
    name = "#{self.id.to_s}.jpg"
    File.open(Rails.root.join('public', 'tmp_avatars/teams', name), 'wb') do |file|
      file.write(avat.read)
    end

    FileUtils.cp "public/tmp_avatars/teams/#{name}", "#{Rails.root}/app/assets/images/avatars/teams/#{name}"
    File.delete("public/tmp_avatars/teams/#{name}") if File.exist?("public/tmp_avatarsteams/#{name}")
    self.avatar = name
  end

  def has_leader?
    self.leader_id.present?
  end

  def leader
    User.find_by_id(self.leader_id)
  end

  def size
    users.count
  end

  def join(user)
    has_leader? ? user.send_team_join_request : join!(user)
  end

  def recalculate_team_rating
    sum = self.users.sum { |u| u.rating }
    # less members bigger rating
    coef = case self.users.size
             when 1..3 then 1
             when 3..10 then 0.9
             when 10..20 then 0.8
             when 20..50 then 0.7
             else 0.6
           end
    self.rating = (sum / self.users.count) * coef
  end

  private

  def join!(user)
    if self.can_accept?(user)
      user.decrease_rep(TEAM_CHANGE_REPUTATION_DECREASE) if user.in_team?
      self.users << user
      self.active = true if self.users.count == USERS_COUNT_TO_BECOME_ACTIVE
      recalculate_team_rating
      self.save!
    end
  end

end
