module Rules
  module GameRules

    def can_start_game?(game)
      unless game.available_sits == 0
        return false
      end

      true
    end

  end
end