module Rules
  module UserRules

    def can_edit?(activity)
      return false unless self.id == activity.id

      true
    end

    def can_edit_team?(team)
      return false unless self.id == team.leader_id

      true
    end

    def can_join?(team)
      return false if joined?(team)
      return false if team.rating - self.rating > 400

      true
    end

    def joined?(team)
      team.users.include? self
    end

    def can_create_news?
      self.settings[:news]
    end

  end
end