module UserExt
  module ProtectedAttributesMethods

    def add_rep(points)

    end

    def decrease_rep(points)
      points ||= 0
      rep = self.reputation - points
      self.transaction do
        sql = "update users set reputation = #{rep} where id = #{self.id}"
        ActiveRecord::Base.connection.execute(sql)
      end

    end

  end
end