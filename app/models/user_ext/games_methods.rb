module UserExt
  module GamesMethods

    def not_started_games
      self.gameplays.select {|g| g if g.game.started_at == nil}
    end

    def has_not_started_games?
      !not_started_games.blank?
    end

    def find_available_one(opts)
      Gameplay.all.detect {|gp| gp if gp.game.started_at == nil && gp.game.cache_get(gp.game.compose_key_for_game_timer) == opts[:timer] &&
          gp.game.min_rating >= opts[:min_rating].to_i && gp.game.max_rating <= opts[:max_rating].to_i && gp.side != opts[:side]}
    end

    def current_games
      self.gameplays.select {|g| g unless g.game.started_at == nil}
    end

    def has_current_games?
      !current_games.blank?
    end

  end
end