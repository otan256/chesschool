module UserExt
  module Registration

    def send_account_activation
      PasswordMailer.account_activation(self).deliver
    end

    def activate_account!
      self.active = true
      self.save!
    end
  end
end