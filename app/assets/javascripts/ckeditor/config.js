CKEDITOR.config.toolbarGroups = [
                { name: 'clipboard',   groups: [ 'undo' ] },
                { name: 'links' },
                { name: 'insert' },
                { name: 'document',    groups: [ 'mode'] },
                { name: 'others' },
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
            ]
