jQuery ->
  c_step = parseInt(getCookie("user_step"));
  switch c_step
    when 1
      $('#actions').show()
    when 2
      $('#step_1').hide()
      $('#c_step_1').hide()
      $('#step_2').show()
      $('#c_step_2').show()
    when 3
      $('#step_1').hide()
      $('#c_step_1').hide()
      $('#step_2').hide()
      $('#c_step_2').hide()
      $('#step_3').show()
      $('#c_step_3').show()

  document.getElementById("continue_registration").onclick = ->
    c_step = parseInt(getCookie("user_step"));
    switch c_step
      when 1 then validateNames(c_step)
      when 2 then validateContacts(c_step)
      when 3 then validateSecurity(c_step)

  # fisrst and last name validations and retrieving
  validateNames = (step) ->
    current_step_el = '#step_' + step
    next_step_el = '#step_' + (step + 1)
    current_coach_el = '#c_step_' + step
    next_coach_el = '#c_step_' + (step + 1)
    first_name = document.getElementById("user_first_name");
    last_name = document.getElementById("user_last_name");
    userData = {'user': {'first_name': first_name.value, 'last_name': last_name.value}}
    if first_name.value.length < 3
      $('#javascript-errors').html("Your first name must have more than 3 letters!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      first_name.style.background = '#F2DEDE'
      first_name.focus()
    else if last_name.value.length < 3
      $('#javascript-errors').html("Your last name must have more than 3 letters!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      last_name.style.background = '#F2DEDE'
      last_name.focus()
    else
      $('#loading').show();
      $.ajax({
        type: "POST",
        data: userData,
        url: "/users",
        success: ->
          $('#loading').fadeOut(1000)
          $(current_step_el).hide()
          $(current_coach_el).hide()
          $(next_step_el).show().addClass('animated bounceInRight')
          $(next_coach_el).show().addClass('animated bounceInLeft')
      })

  # phone and e-mail validations and retrieving them
  validateContacts = (step) ->
    phone = document.getElementById("user_phone");
    email = document.getElementById("user_email");
    current_step_el = '#step_' + step
    next_step_el = '#step_' + (step + 1)
    current_coach_el = '#c_step_' + step
    next_coach_el = '#c_step_' + (step + 1)
    userData = {'user': {'phone': phone.value, 'email': email.value}}
    if validatePhone(phone.value)
      $('#javascript-errors').html("Your phone number is not valid! Phone must contatin 10 digits!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      phone.style.background = '#F2DEDE'
      phone.focus()
    else if validateEmail(email.value)
      $('#javascript-errors').html("Your email is not valid! Please enter valid email adress!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      email.style.background = '#F2DEDE'
      email.focus()
    else
      $('#loading').show();
      $.ajax({
        type: "POST",
        data: userData,
        url: "/users",
        success: ->
          $('#loading').fadeOut(1000);
          $(current_step_el).hide()
          $(current_coach_el).hide()
          $(next_step_el).show().addClass('animated bounceInRight')
          $(next_coach_el).show().addClass('animated bounceInLeft')
      })

  # password length, password confirmation and security code validations
  validateSecurity = (step) ->
    password = document.getElementById("user_password");
    password_confirmation = document.getElementById("user_password_confirmation");
    security_code = document.getElementById("security_code").value;
    userData = {'user': {'password': password.value, 'security_code': security_code}}
    if validatePassword(password.value)
      $('#javascript-errors').html("Your password is not valid! Password must contain at least one number and one letter").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      password.style.background = '#F2DEDE'
      password.focus()
    else if password.value != password_confirmation.value
      $('#javascript-errors').html("Your password do not match password confirmation that you entered! Please reenter them!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
      password_confirmation.style.background = '#F2DEDE'
      password_confirmation.focus()
    else
      $('#loading').show();
      $.ajax({
        type: "POST",
        data: userData,
        url: "/users",
        success: ->
          $('#loading').fadeOut(1000)
        error: ->
          $('#loading').fadeOut(1000)

      })

  validateEmail = (email) ->
    re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !re.test(email);

  validatePhone = (phone) ->
    re = /^(\+91-|\+91|0)?\d{10}$/
    return !re.test(phone)

  validatePassword = (password) ->
    res = (password.search(/[a-zA-Z]+/) == -1) || (password.search(/[0-9]+/) == -1)
    return !res

# needed to get user_step from cookies
getCookie = (c_name) ->
  c_value = document.cookie;
  c_start = c_value.indexOf(" " + c_name + "=");
  if c_start == -1
    c_start = c_value.indexOf(c_name + "=");
  if c_start == -1
    c_value = null;
  else
    c_start = c_value.indexOf("=", c_start) + 1;
  c_end = c_value.indexOf(";", c_start);
  if c_end == -1
    c_end = c_value.length;

  c_value = unescape(c_value.substring(c_start, c_end));

  return c_value;
