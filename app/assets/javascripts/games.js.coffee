# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('.best_in_place').best_in_place()
  document.getElementById("create_game").onclick = ->
    min = parseInt(document.getElementById("game_min_rating").value);
    max = parseInt(document.getElementById("game_max_rating").value);
    timer = document.getElementById("game_timer").value;
    side = document.getElementById("game_side").value;
    gameData = {'max_rating': max, 'min_rating': min, 'timer': timer, "side": side}
    if max <= min
      $('#javascript-errors').html("Maximum rating cannot be less than minimum! Please be more patient!").show().addClass('animated bounceInRight').delay(5000).slideUp(700);
    else
      $('#loading').show();
      $.ajax({
        type: "POST",
        data: gameData,
        url: "/games",
        success: ->
          $('#javascript-info').html("Game was successfully created!").show().addClass('animated bounceInUp').delay(5000).slideUp(700);
          $('#loading').fadeOut(1000);
        error: ->
          $('#javascript-info').html("Game was successfully created and started!").show().addClass('animated bounceInUp').delay(5000).slideUp(700);
          $('#loading').fadeOut(1000);
      })

