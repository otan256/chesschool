module GamesHelper

  def side_tag(side, options = {})
    size = options[:small] ? '35x35' : '50x50'
    size = '20x20' if options[:tiny]
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    image_tag(side, size: size, class: clazz, id: 'picture')
  end

  def chess_board_tag(side, options ={})
    size = options[:large] ? '600x600' : '400x400'
    size = '200x200' if options[:small]

    image_tag("#{side.downcase}_board_main.png", size: size, class: 'img-polaroid')
  end


end
