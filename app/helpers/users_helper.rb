module UsersHelper

  def avatar_tag(user, options = {})
    size = options[:small] ? '70x70' : '200x200'
    size = '30x30' if options[:tiny]
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    link_to image_tag("avatars/users/#{user.avatar}", size: size, class: clazz), user_path(user.id),
            'rel' => 'tooltip', title: "#{user.full_name}"
  end

  def username(user)
    link_to user.full_name, user_path(user.id)
  end

  def info_tag(hash)
    hint = '(This parameter is not valid.)' unless valid_one? hash
    hash[:p] ? content_tag(:div, hash[:p], class: 'user-info') + content_tag(:div, hint, class: 'user-info-hint') : t('globals.empty')
  end

  def email_tag(email)
    hint = '(This parameter is not valid.)'
    email =~ /^.+@.+$/ ? email : email + content_tag(:div, hint, class: 'user-info-hint')
  end

  def valid_one?(hash)
    if hash[:key] == :phone
      return hash[:p] =~ /(?:\+?|\b)[0-9]{10}\b/
    end
    true
  end

  def sent_at(m)
    distance_of_time_in_words(Time.now, m.created_at) + t('globals.ago')
  end

end
