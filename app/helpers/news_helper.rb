module NewsHelper

  def attachment_tag(post, options = {})
    size = options[:small] ? '70x70' : '200x200'
    size = '500x500' if options[:huge]
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    image_tag("files/news/#{post.attachments}", size: size, class: clazz)

  end

end
