module TeamsHelper

  def team_avatar_tag(team, options = {})
    size = options[:small] ? '70x70' : '200x200'
    size = '30x30' if options[:tiny]
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    link_to image_tag("avatars/teams/#{team.avatar}", size: size, class: clazz), team_path(team.id)
  end

  def inactive_name(team)
    content_tag(:div, "#{team.name} (inactive)", class: 'team-hint')
  end

end
