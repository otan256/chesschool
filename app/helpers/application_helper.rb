module ApplicationHelper

  include RedisSupport::RedisHelper

  def copyright_notice_year_range(start_year)
    # In case the input was not a number (nil.to_i will return a 0)
    start_year = start_year.to_i

    # Get the current year from the system
    current_year = Time.new.year
    # When the current year is more recent than the start year, return a string
    # of a range (e.g., 2010 - 2012). Alternatively, as long as the start year
    # is reasonable, return it as a string. Otherwise, return the current year
    # from the system.
    if current_year > start_year && start_year > 2000
      "#{start_year} - #{current_year}"
    elsif start_year > 2000
      "#{start_year}"
    else
      "#{current_year}"
    end
  end

  ###
  # coach types => victor, aleksander, robert
  ###

  def coach_box(text, coach_type = :victor, options = {})
    options[:notice_type] ||= :notice
    clazz = options[:rounded] ? 'img-circle' : 'img-polaroid'

    content_tag(:div, content_tag(:div, link_to(image_tag("avatars/chessmaster.jpg", size: '70x70', class: clazz), '#', class: 'avatar') +
        content_tag(:div, link_to(t("globals.#{coach_type}"), '#'), class: 'name'), class: 'coach') +
        content_tag(:div, text, class: 'coach_text'), class: options[:notice_type])
  end

  def online_count
    redis_count = cache_get(:users_online_count.to_s)
    last_updated_redis_count = cache_get(:last_updated_users_online_count.to_s)
    if redis_count && last_updated_redis_count && last_updated_redis_count.to_time < 10.minutes.ago
      redis_count
    else
      sql_count = User.count conditions: ['updated_at > ?', Time.now.utc - 10.minutes]
      cache_put :users_online_count.to_s, sql_count
      cache_put :last_updated_redis_count.to_s, Time.now.to_s
      sql_count
    end
  end

  def portal_dashboard_link(params)
    portal_site = Rails.env == 'development' ? 'http://localhost:3001' : 'http://dygabox.herokuapp.com'
    portal_site + '/portal/dashboard' + '?' + "tab=#{params[:tab]}&what=#{params[:what]}"
  end

end
