class InvitationsMailer < ActionMailer::Base
  default from: "chesschool@support.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mailer.invitation.subject
  #
  def invitation(invitation, path)
    @url = 'http://chesschool.herokuapp.com' + path
    @invitor = invitation.sender
    mail to: invitation.recipient_email, subject: t('mailer.invitations.subject')
    invitation.update_attribute(:sent_at, Time.now)
  end
end
