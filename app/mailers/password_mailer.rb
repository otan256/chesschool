class PasswordMailer < ActionMailer::Base

  default from: "chesschool@support.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def account_activation(user)
    @user = user
    mail to: user.email, subject: t('mailer.account_activation.subject')
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: t('mailer.password_reset.subject')
  end

end