class WelcomeController < ApplicationController

  include Extensions::LoginExt
  include Extensions::PortalIntegration

  before_filter :authorize_access, :except => [:logout, :index, :do_login]

  def authorize_access
    unless logged_in?
      redirect_to_with_notice root_path, t('notices.login.authorization_required'), :error
    end
  end

  def index
    @invitation = Invitation.new
    redirect_if_logged_in
    @user = User.new
  end

  def logout
    reset_user_session
    reset_session

    redirect_to root_path
  end

end
