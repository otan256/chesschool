class GamesController < WelcomeController

  include Extensions::GamesExt

  before_filter :authorize_access
  before_filter :get_game_data, :only => [:show, :make_move]
  after_filter :save_game_dump, :only => [:show, :make_move]

  caches_action :index

  # GET /games
  # GET /games.json
  def index
    @games = Game.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @games }
      format.js
    end
  end

  # GET /games/1
  # GET /games/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @game_db }
    end
  end

  # GET /games/new
  # GET /games/new.json
  def new
    @game = Game.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @game }
    end
  end

  # GET /games/1/edit
  def edit
    @game = Game.find(params[:id])
  end


  # POST /games
  # POST /games.json
  def create
    params.except!(:action, :controller)
    @game = current_user.find_available_one(params) # actually this is instance of a Gameplay not a Game

    @game ||= Game.new(params.except(:side, :timer))

    if @game.new_record?
      respond_to do |format|
        if @game.create_available(params, current_user)
          expire_action action: :index
          format.html do
            if request.xhr?
              render :json => {:result => "ok"}
            else
              redirect_to games_path
            end
          end
          format.json { render json: @game, status: :created, location: @game }
        else
          format.html
          format.json { render json: @game.errors, status: :unprocessable_entity }
        end
      end
    else
      # available gameplay found. doing join and start
      @game_core = @game.game.force_join!(current_user).start!
      render js: "window.location = '/games/#{@game.game_id}';"
    end
  end

  # PUT /games/1
  # PUT /games/1.json
  def update
    @game = Game.find(params[:id])

    respond_to do |format|
      if @game.update_available(params[:game])
        format.html { redirect_to games_path, notice: 'Game was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @gameplay = Gameplay.find(params[:id])
    @gameplay.destroy
    expire_action action: :index

    respond_to do |format|
      format.js
    end
  end
end
