class UsersController < WelcomeController

  include Extensions::RegisterExt

  before_filter :authorize_access, except: [:new, :create, :index]
  caches_action :index
  caches_action :show, layout: false

  # GET /users
  # GET /users.json
  def index
    clear_cache if params[:cancel_register]
    authorize_access
    @users = User.all
    @online_users = User.online_users
    @message = Message.new
    @user = User.find_by_id params[:receiver_id].to_i

    #respond_to do |format|
    #  format.html
    #  format.js
    #end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @message = Message.new
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
      format.js
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    check_if_steps_overloaded
    cookies[:user_step] ||= 1
    session[:user_params] ||= {}
    @user = User.new(session[:user_params])
    @user.current_step = cookies[:user_step]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
      #format.js
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    prepare_user_for_create
    if cookies[:user_step].to_i == 3
      @user.password = User.encrypt_a_password(params[:user][:password])
      respond_to do |format|
        if check_security
          if @user.save!
            @user.activate_account!
            expire_action action: :index
            increase_step
            format.js { render :action => 'show_final', :layout => false }
          else
            format.js { render :action => 'error_s', :layout => false }
          end
        else
          format.js { render :action => 'error', :layout => false }
        end
      end
    else
      set_and_send_security if cookies[:user_step].to_i == 2
      increase_step
    end
    respond_to do |format|
      format.html { redirect_to new_user_path, remote: true }
    end
  end


# PUT /users/1
# PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_edited(prepare_user_for_update)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def prepare_user_for_create
    session[:user_params].deep_merge!(params[:user]) if params[:user]
    @user = User.new(session[:user_params])
  end

  def increase_step
    @user.current_step = cookies[:user_step].to_i
    cookies[:user_step] = @user.next_step
    @user.current_step = cookies[:user_step].to_i
  end

  def prepare_user_for_update
    params[:user] ? params[:user] : {about: 'Trying to become incognito?'}
  end

end
