class PasswordRestoreController < ApplicationController

  def new
  end

  def create
    user = User.find_by_email(params[:restore][:email])
    user.send_password_reset if user
    redirect_to_with_notice root_url, t('welcome.registration.password_restore.email_sent_reset')
  end

  def edit
    @user = User.find_by_password_reset_token!(params[:id])
  end

  def update
    @user = User.find_by_password_reset_token!(params[:id])
    unless @user.from_facebook?
      if params[:user][:confirm_password] == params[:user][:password]
        params[:user].delete :confirm_password
        if @user.password_reset_sent_at < 2.hours.ago
          redirect_to_with_notice root_path, t('welcome.registration.password_restore.pass_expired'), :error
        elsif @user.reset_password!(params[:user])
          redirect_to_with_notice root_url, t('welcome.registration.password_restore.pass_reset'), :success
        else
          render :edit
        end
      else
        redirect_to_with_notice root_path, t('welcome.registration.password_restore.confirmation_not_match'), :error
      end
    end
  end

end
