class InvitationsController < WelcomeController

  def create
    @invitation = Invitation.new(params[:invitation])
    @invitation.sender = current_user

    if @invitation.save
      if logged_in?
        InvitationsMailer.invitation(@invitation, sign_up_path(@invitation.token)).deliver
        redirect_to_with_notice users_path, t('notices.invitations.success_sent'), :success
      else
        redirect_to_with_notice root_url, t('notices.invitations.not_logged_in')
      end
    else
      redirect_to_with_notice user_path(current_user), @invitation.errors.full_messages.first, :error
    end
  end

end
