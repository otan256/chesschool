module Extensions
  module RegisterExt

    def set_and_send_security
      @user = User.new(session[:user_params].deep_merge!(security_code: @user.generate_security_code))
      raise 'SECURITY CODE NOT SET FOR USER' unless @user.security_code
      @user.send_account_activation
    end

    def check_security
      @user.valid_security_code?(params[:user][:security_code].strip)
    end

    def check_if_saved
      clear_cache
      respond_to do |format|
        format.js { render :action => 'error', :layout => false }
      end
    end

    def check_if_steps_overloaded
      if cookies[:user_step].to_i > 4
        cookies.delete(:user_step)
        cookies[:user_step] = 1
      end
    end

    def failed_to_register?
      params[:failed]
    end

    def clear_cache
      cookies.delete(:user_step)
      session.delete :user_params
    end

  end
end