module Extensions
  module GamesExt

    def make_move
      @game_core.board.play_move!(params[:move])
      save_game_dump
      redirect_to game_path(@game_db)
    end

    private

    def get_user
      @user = current_user
    end

    def get_game_data
      @game_db = Game.find_by_id(params[:id])
      redirect_to_with_notice games_path, t('notices.games.no_such_game') and return if @game_db.nil?
      @game_db.board ? @game_core = Chess::GameCore.restore_from_dump(@game_db.board) : raise('GAME BOARD IS EMPTY')
    end

    def save_game
      Game.transaction do
        if @game_core.finished?
          @game_db.finished_at = DateTime.now
          distribute_rating
        end

        @game_db.board = @game_core.board
        @game_db.save!
      end
    end

    def save_game_dump
      #Game.transaction do
        @game_db.board = @game_core.board
        @game_db.save!
      #end
    end

    def distribute_rating
    end



  end
end