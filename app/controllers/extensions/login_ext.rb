module Extensions::LoginExt

  def do_login

    email = params[:email]
    password = params[:password]
    url = games_path

    unless email.blank? || password.blank?
      users_array = User.find_by_email(email).try(:authenticate, email, password)
      if users_array && !users_array.blank?
        #send_mail(user)
        redirect_to_with_notice url, t('notices.login.success', username: users_array.first.first_name) if login_user(users_array.first)
      end
    end
    redirect_to_with_notice root_path, t('notices.login.invalid_login_or_password'), :error
  end

  private

  def login_user(user)
    return false unless user.active?

    if params[:remember_me]
      cookies.permanent[:remember_me_token] = user.remember_me_token
    else
      cookies[:remember_me_token] = { value: user.remember_me_token, expires: 1.hour.from_now }
    end
    true
  end

end