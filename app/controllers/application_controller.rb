class ApplicationController < ActionController::Base
  protect_from_forgery

  after_filter :user_activity

  # Authorized user
  def current_user
    return @current_user if defined?(@current_user)
    return nil if cookies[:remember_me_token].blank?

    @current_user = User.find_by_remember_me_token(cookies[:remember_me_token])
    @current_user.save! if @current_user
    @current_user
  end
  helper_method :current_user

  def current_url
    url_for(only_path: false, overwrite_params: nil)
  end
  helper_method :current_url

  def logged_in?
    current_user.present?
  end
  helper_method :logged_in?

  def reset_user_session
    cookies[:remember_me_token] = nil
    cookies.permanent[:remember_me_token] = nil
  end

  def redirect_to_with_notice(options = { }, notice = nil, type = :notice)
    redirect_to options, flash: { type => notice }
  end

  # we redirect only first time
  def redirect_to(options = { }, response_status = { }) #:doc:
    unless response_body
      super(options, response_status)
    end
  end

  def redirect_if_logged_in
    redirect_to user_path(current_user.id) if logged_in?
  end

  private

  def user_activity
    current_user.try :update_online
  end

end
