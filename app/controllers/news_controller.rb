class NewsController < WelcomeController
  rescue_from ActiveRecord::RecordInvalid, with: :record_not_valid
  
  before_filter :authorize_access
  caches_action :index
  # GET /news
  # GET /news.json
  def index
    @post = News.new
    @show_post = News.all.first
    @all = News.order('id desc')
    @recent = News.recent.order('id desc')
    @interesting = News.interesting.order('id desc')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @all_news }
      format.js
    end
  end

  # POST /news
  # POST /news.json
  def create
    @post = News.new()
    
    respond_to do |format|
      if @post.save_one!(params[:news])
        expire_action action: :index
        format.html { redirect_to_with_notice news_index_path, t('notices.news.created') }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: :index }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news = News.find(params[:id])
    @news.destroy
    expire_action action: :index

    respond_to do |format|
      format.html { redirect_to news_index_url }
      format.json { head :no_content }
    end
  end

  # POST /rate_news
  # POST /rate_news.json
  def rate
    @post = News.find(params[:id])
    @post.like!(params[:stars], @post, params[:dimension])

    render :update do |page|
      page.replace_html @post.wrapper_dom_id(params), ratings_for(@post, params.merge(:wrap => false))
      page.visual_effect :highlight, @post.wrapper_dom_id(params)
    end
  end
  
  private
  
  def record_not_valid
    respond_to do |format|
      format.html {redirect_to_with_notice news_index_path, t('notices.news.not_valid_error'), :error }
    end
  end  
end
