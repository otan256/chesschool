class TeamsController < WelcomeController

  before_filter :authorize_access
  before_filter :set_team, only: [:show, :update, :join]
  caches_action :index
  caches_action :show, layout: false

  # GET /teams
  # GET /teams.json
  def index
    @team = Team.new
    @teams = Team.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @teams }
    end
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @team }
    end
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(params[:team])

    respond_to do |format|
      if @team.save_team(params[:team])
        expire_action action: :index
        current_user.force_join_to_team!(@team)
        format.html { redirect_to_with_notice @team, t('notices.teams.team_created'), :success }
        format.json { render json: @team, status: :created, location: @team }
      else
        format.html { render action: "index" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /teams/1
  # PUT /teams/1.json
  def update
    redirect_to_with_notice @team, t('notices.teams.cannot_edit'), :error unless current_user.can_edit_team? @team

    respond_to do |format|
      if @team.save_team(params[:team])
        format.html { redirect_to_with_notice @team, t('notices.teams.team_updated'), :success }
        format.json { head :no_content }
      else
        format.html { render action: "show" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # Get /teams/join/1
  def join
    notice = @team.has_leader? ? t('notices.teams.request_for_join_sent') : t('notices.teams.joined')

    respond_to do |format|
      if @team.join(current_user)
        format.html { redirect_to_with_notice @team, notice}
        format.json { head :no_content }
      else
        format.html { redirect_to_with_notice teams_path, t('notices.teams.cannot_join'), :error }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_team
    @team = Team.find(params[:id]) if params[:id]
    redirect_to_with_notice teams_path, t('notices.teams.team_not_found'), :error unless @team
  end

end
