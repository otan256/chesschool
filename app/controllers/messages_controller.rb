class MessagesController < WelcomeController

  before_filter :authorize_access

  def index
    @message = Message.new

    respond_to do |format|
      format.html
      format.js {render m: params[:m]}
    end
  end

  def show
    @m = Message.find_by_id params[:id]
    respond_to do |format|
      format.html {render :index, remote: true, m: @m}
    end
  end

  def create
    @message = Message.new()
    redirect_url = params[:message][:url]

    respond_to do |format|
      if @message.save_one!(params[:message])
        format.html {redirect_to_with_notice redirect_url, t('notices.messages.success_sent'), :success}
      else
        format.html {redirect_to_with_notice redirect_url, t('notices.messages.error_sent'), :error}
      end
    end
  end

  def destroy
    @message = Message.find_by_id params[:id]
    @message.destroy

    respond_to do |format|
      format.html { redirect_to messages_url(tab: :inbox) }
      format.json { head :no_content }
    end
  end

end