Chesschool::Application.routes.draw do

  match '/rate' => 'rater#create', :as => 'rate'

  mount Ckeditor::Engine => '/ckeditor'

  resources :invitations


  root to: 'welcome#index'

  match '/logout' => 'welcome#logout', :as => :logout
  # replace localhost:3001 with dygabox.herokuapp.com for production
  match '/portal_authorize' => redirect('http://localhost:3001/api/get_user?source=chess'), as: :portal_auth
  match '/signup/:invitation_token', controller: :users, action: :new, via: :get, as: :sign_up

  resources :password_restore

  resources :welcome do
    collection do
      get :about
      post :do_login
      get :forgot
      get 'activate/(:token)' => 'welcome#activate', as: :activate
    end
  end

  resources :users

  resources :messages

  resources :games do
    member do
      get 'make_move/(:id)' => 'games#make_move', as: :make_move
    end
  end

  resources :teams, :shallow => true do
    member do
      post :rate
      get :join
    end

  end

  resources :news do
    collection do
      post 'rate/(:id)' => 'news#rate', as: :rate
    end
  end


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
